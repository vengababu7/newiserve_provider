//
//  PickUpViewController.m
//  DoctorMapModule
//
//  Created by Rahul Sharma on 04/02/14.
//  Copyright (c) 2014 Rahul Sharma. All rights reserved.
//

#import "PickUpViewController.h"
//#import "SelectedAddress.h"
#import "ManageAddress.h"

@interface PickUpViewController ()
{
    NSMutableArray *previouslySelectedAddress;
    NSMutableArray *managedAddress;
}

@property(nonatomic,assign) BOOL isSearchResultCome;
@property NSTimer *autoCompleteTimer;
@property NSString *substring;
@property NSMutableArray *pastSearchWords;
@property NSMutableArray *pastSearchResults;

@end

@implementation PickUpViewController

@synthesize latitude;
@synthesize longitude;
@synthesize locationType;
@synthesize isSearchResultCome;


#pragma mark - Initial Methods -

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self)
    {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.mAddress = [NSMutableArray array];
    
    //    self.searchBarController.searchBarStyle = UISearchBarStyleProminent;
    
    self.pastSearchWords = [NSMutableArray array];
    self.pastSearchResults = [NSMutableArray array];
    
    [self createNavLeftButton];
}

-(void)viewDidAppear:(BOOL)animated{
    
    [_searchBarController becomeFirstResponder];
}

-(void)viewWillDisappear:(BOOL)animated{
    
    [self.view endEditing:YES];
}
/*-----------------------------------*/
#pragma mark - Navigation Bar methods
/*-----------------------------------*/
- (void)createNavLeftButton
{
    UIButton *navCancelButton =  [UIButton buttonWithType:UIButtonTypeCustom];
    [navCancelButton setImage:[UIImage imageNamed:@"current_booking_cross_icon_off"]
                     forState:UIControlStateNormal];
    [navCancelButton setImage:[UIImage imageNamed:@"current_booking_cross_icon_on"]
                     forState:UIControlStateSelected];
    
    [navCancelButton addTarget:self action:@selector(backToController) forControlEvents:UIControlEventTouchUpInside];
    [navCancelButton setFrame:CGRectMake(10.0f,0.0f,40,40)];
    
    // Create a container bar button
    UIBarButtonItem *containingcancelButton = [[UIBarButtonItem alloc] initWithCustomView:navCancelButton];
    
    UIBarButtonItem *negativeSpacer = [[UIBarButtonItem alloc]
                                       initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace
                                       target:nil action:nil];
    negativeSpacer.width = -16;// it was -6 in iOS 6
    [self.navigationItem setLeftBarButtonItems:[NSArray arrayWithObjects:negativeSpacer,containingcancelButton, nil] animated:NO];
}

-(void)backToController
{
    [self.view endEditing:YES];
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (void)refresh:(UIRefreshControl *)refreshControl
{
    [refreshControl endRefreshing];
}

#pragma mark - UIButton Actions -

- (IBAction)navigationBackButtonAction:(id)sender
{
    [self.view endEditing:YES];
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark  - UITableView DataSource -

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    if(self.searchBarController.text.length == 0)
    {
        return 2;
    }
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if(self.searchBarController.text.length == 0)
    {
        if(section == 0)
        {
            return managedAddress.count;
        }
        else
        {
            return previouslySelectedAddress.count;
        }
    }
    return self.mAddress.count;
}

-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"Cell";
    UITableViewCell *cell=nil;
    tableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
    tableView.backgroundColor=[UIColor clearColor];
    
    if(cell == nil)
    {
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:cellIdentifier];
        cell.selectionStyle=UITableViewCellSelectionStyleGray;
        cell.backgroundColor=[UIColor clearColor];
        cell.textLabel.font = [UIFont fontWithName:@"OpenSans" size:14];
        cell.textLabel.textColor = UIColorFromRGB(0x4A4971);
        cell.detailTextLabel.font = [UIFont fontWithName:@"OpenSans" size:12];
        cell.detailTextLabel.textColor = UIColorFromRGB(0x7e7e7e);
        cell.detailTextLabel.numberOfLines = 0;
    }
    NSDictionary *searchResult = [self.mAddress objectAtIndex:indexPath.row];
    cell.textLabel.text = [searchResult[@"terms"] objectAtIndex:0][@"value"];
    cell.detailTextLabel.text = searchResult[@"description"];
    
    return cell;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *cellIdentifier = @"Cell";
    UITableViewCell *cell=nil;
    tableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
    tableView.backgroundColor=[UIColor clearColor];
    
    if(cell == nil)
    {
        cell =[[UITableViewCell alloc]initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:cellIdentifier];
        cell.selectionStyle=UITableViewCellSelectionStyleGray;
        cell.backgroundColor = [UIColor clearColor];
        
        cell.textLabel.font = [UIFont fontWithName:@"OpenSans" size:14];
        cell.textLabel.textColor = UIColorFromRGB(0x4A4971);
        cell.textLabel.numberOfLines = 0;
        
        cell.detailTextLabel.font = [UIFont fontWithName:@"OpenSans" size:12];
        cell.detailTextLabel.textColor = UIColorFromRGB(0x7e7e7e);
        cell.detailTextLabel.numberOfLines = 0;
    }
    
    float height2;
    
        NSDictionary *searchResult = [self.mAddress objectAtIndex:indexPath.row];
        cell.textLabel.text = [searchResult[@"terms"] objectAtIndex:0][@"value"];
        cell.detailTextLabel.text = searchResult[@"description"];
        height2 = [self measureHeightLabel:cell.detailTextLabel];
    
    
    return height2+20;
}

- (CGFloat)measureHeightLabel: (UILabel *)label
{
    CGSize constrainedSize = CGSizeMake(self.view.frame.size.width-30  , 9999);
    NSDictionary *attributesDictionary = [NSDictionary dictionaryWithObjectsAndKeys:
                                          [UIFont fontWithName:label.font.fontName size:label.font.pointSize], NSFontAttributeName,
                                          nil];
    
    NSMutableAttributedString *string = [[NSMutableAttributedString alloc] initWithString:label.text attributes:attributesDictionary];
    
    CGRect requiredHeight = [string boundingRectWithSize:constrainedSize options:NSStringDrawingUsesLineFragmentOrigin context:nil];
    
    CGRect newFrame = label.frame;
    newFrame.size.height = requiredHeight.size.height;
    return  newFrame.size.height;
}

//-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
//{
//    UIView *headerView = [[UIView alloc]initWithFrame:CGRectMake(0, 0,CGRectGetWidth(tableView.frame), 25)];
//    UILabel *titleLabel = [[UILabel alloc]initWithFrame:CGRectMake(10, 2, CGRectGetWidth(tableView.frame)-20, 20)];
//
//    [headerView addSubview:titleLabel];
//
//    NSString *title;
//
//    if(isManageAddressAvailable)
//    {
//        title = @"Searched Addresses";
//    }
//    else if (self.mAddress.count == 0)
//    {
//        title = @"No Searched Address";
//        titleLabel.textAlignment = NSTextAlignmentCenter;
//    }
//    else
//    {
//        title = @"Result";
//    }
//
//    titleLabel.text = title;
//
//    titleLabel.font = [UIFont fontWithName:OpenSans_SemiBold size:13];
//    titleLabel.textColor = UIColorFromRGB(0x333333);
//
//    return headerView;
//}

-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    
    return 0;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    
    //    if (section == 0) {
    //        return 25;
    //    }
    return 0;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
        NSDictionary *searchResult;
        searchResult = [self.mAddress objectAtIndex:indexPath.row];
        
        NSString *placeID = [searchResult objectForKey:@"reference"];
        [self.searchBarController resignFirstResponder];
        
        
        [self retrieveJSONDetailsAbout:placeID
                        withCompletion:^(NSArray *place) {
                            
                            if (self.pickUpAddressDelegate && [self.pickUpAddressDelegate respondsToSelector:@selector(getAddressFromPickUpAddressVC:)])
                            {
                                
                                NSDictionary *dict = [NSDictionary dictionary];
                                
                                NSString *add1 = [NSString stringWithFormat:@"%@",[place valueForKey:@"name"]];
                                NSString *add2 = [NSString stringWithFormat:@"%@",[place valueForKey:@"formatted_address"]];
                                if (add1.length == 0) {
                                    
                                    add1 = [add1 stringByAppendingString:[place valueForKey:@"formatted_address"]];
                                    add2 = @"";
                                }
                                
                                NSString *late = [NSString stringWithFormat:@"%@,",[place valueForKey:@"geometry"][@"location"][@"lat"]];
                                NSString *longi = [NSString stringWithFormat:@"%@",[place valueForKey:@"geometry"][@"location"][@"lng"]];
                                
                                dict = @{
                                         @"address1":add1,
                                         @"address2":add2,
                                         @"lat":late,
                                         @"long":longi,
                                         };
                                
                                [self.pickUpAddressDelegate getAddressFromPickUpAddressVC:dict];
                            }
                            [self navigationBackButtonAction:nil];
                        }];
        
}



#pragma mark - Search Bar Delegates -
#pragma mark  Autocomplete SearchBar methods -

-(void)searchBarSearchButtonClicked:(UISearchBar *)searchBar{
    
    [self.autoCompleteTimer invalidate];
    [self searchAutocompleteLocationsWithSubstring:self.substring];
    [self.searchBarController resignFirstResponder];
    [self.tblView reloadData];
}

-(void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText{
    
    NSString *searchWordProtection = [self.searchBarController.text stringByReplacingOccurrencesOfString:@" " withString:@""];
    if (searchWordProtection.length > 0) {
        
        [self runScript];
        
    } }

-(BOOL)searchBar:(UISearchBar *)searchBar shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    
    self.substring = [NSString stringWithString:self.searchBarController.text];
    self.substring= [self.substring stringByReplacingOccurrencesOfString:@" " withString:@"+"];
    self.substring = [self.substring stringByReplacingCharactersInRange:range withString:text];
    
    if ([self.substring hasPrefix:@"+"] && self.substring.length >1) {
        self.substring  = [self.substring substringFromIndex:1];
    }
    return YES;
}

- (void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar
{
    //    [searchBar setShowsCancelButton:YES animated:YES];
    
}

- (BOOL)searchBarShouldEndEditing:(UISearchBar *)searchBar
{
    return YES;
}

-(void)searchBarCancelButtonClicked:(UISearchBar *)searchBar{
    
    //    [self.view endEditing:YES];
}

- (void)runScript{
    
    [self.autoCompleteTimer invalidate];
    self.autoCompleteTimer = [NSTimer scheduledTimerWithTimeInterval:0.15f
                                                              target:self
                                                            selector:@selector(searchAutocompleteLocationsWithSubstring:)
                                                            userInfo:nil
                                                             repeats:NO];
}

- (void)searchAutocompleteLocationsWithSubstring:(NSString *)substring
{
    //    isManageAddressAvailable = NO;
    
    
    //
    
    if (![self.pastSearchWords containsObject:self.substring]) {
        
        //        [[ProgressIndicator sharedInstance] showPIOnView:[[UIApplication sharedApplication] keyWindow]
        //                                                     withMessage:@"Loading..."];
        
        [self.mAddress removeAllObjects];
        [self.tblView reloadData];
        
        [self.pastSearchWords addObject:self.substring];
        [self retrieveGooglePlaceInformation:self.substring
                              withCompletion:^(NSArray * results) {
                                  
                                  if(results)//GOT RESULTS
                                  {
                                      //                                [[ProgressIndicator sharedInstance]hideProgressIndicator];
                                      [self.mAddress addObjectsFromArray:results];
                                      //                                  isSearchResultCome = YES;
                                      NSDictionary *searchResult = @{@"keyword":self.substring,@"results":results};
                                      [self.pastSearchResults addObject:searchResult];
                                      [self.tblView reloadData];
                                  }
                                  else
                                  {
                                      
                                  }
                                  
                              }];
        
    }else if([self.pastSearchWords containsObject:self.substring]) {
        
        [self.mAddress removeAllObjects];
        for (NSDictionary *pastResult in self.pastSearchResults) {
            if([[pastResult objectForKey:@"keyword"] isEqualToString:self.substring]){
                [self.mAddress addObjectsFromArray:[pastResult objectForKey:@"results"]];
                [self.tblView reloadData];
            }
        }
    }
}


#pragma mark - Google API Requests -


-(void)retrieveGooglePlaceInformation:(NSString *)searchWord
                       withCompletion:(void (^)(NSArray *))complete {
    
    
    NSString *searchWordProtection = [searchWord stringByReplacingOccurrencesOfString:@" " withString:@""];
    
    if (searchWordProtection.length != 0) {
        
        NSString *urlString = [NSString stringWithFormat:@"https://maps.googleapis.com/maps/api/place/autocomplete/json?input=%@&types=establishment|geocode&location=%@,%@&radius=500po &language=en&key=%@",searchWord,latitude,longitude,KPMDServerKey];
        
        NSLog(@"AutoComplete URL: %@",urlString);
        
        
        NSURL *url = [NSURL URLWithString:[urlString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
        NSURLRequest *request = [NSURLRequest requestWithURL:url];
        
        NSURLSession *delegateFreeSession;
        delegateFreeSession = [NSURLSession sessionWithConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]
                                                            delegate:nil
                                                       delegateQueue:[NSOperationQueue mainQueue]];
        
        NSURLSessionDataTask *task;
        task = [delegateFreeSession dataTaskWithRequest:request
                                      completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                          
                                          
                                          if (!data || !response || error) {
                                              NSLog(@"Google Service Error : %@",[error localizedDescription]);
                                              return;
                                          }
                                          
                                          NSDictionary *jSONresult = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:nil];
                                          NSArray *results = [jSONresult valueForKey:@"predictions"];
                                          
                                          if (error || [jSONresult[@"status"] isEqualToString:@"NOT_FOUND"] || [jSONresult[@"status"] isEqualToString:@"REQUEST_DENIED"]) {
                                              
                                              complete(nil);
                                          }
                                          else
                                          {
                                              complete(results);
                                          }
                                      }];
        [task resume];
    }
    
}

-(void)retrieveJSONDetailsAbout:(NSString *)place withCompletion:(void (^)(NSArray *))complete {
    
    NSString *urlString = [NSString stringWithFormat:@"https://maps.googleapis.com/maps/api/place/details/json?reference=%@&key=%@",place,KPMDServerKey];
    
    NSURL *url = [NSURL URLWithString:urlString];// stringByAddingPercentEscapesUsingEncoding: NSUTF8StringEncoding]];
    NSURLSessionConfiguration *defaultConfigObject = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession *delegateFreeSession = [NSURLSession sessionWithConfiguration: defaultConfigObject delegate: nil delegateQueue: [NSOperationQueue mainQueue]];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    NSURLSessionDataTask *task = [delegateFreeSession dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        NSDictionary *jSONresult = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:nil];
        NSArray *results = [jSONresult valueForKey:@"result"];
        
        if (error || [jSONresult[@"status"] isEqualToString:@"NOT_FOUND"] || [jSONresult[@"status"] isEqualToString:@"REQUEST_DENIED"]){
            if (!error){
                NSDictionary *userInfo = @{@"error":jSONresult[@"status"]};
                NSError *newError = [NSError errorWithDomain:@"API Error" code:666 userInfo:userInfo];
                complete(@[@"API Error", newError]);
                return;
            }
            complete(@[@"Actual Error", error]);
            return;
        }else{
            complete(results);
        }
    }];
    
    [task resume];
}

@end
