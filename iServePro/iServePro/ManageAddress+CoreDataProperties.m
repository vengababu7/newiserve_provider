//
//  ManageAddress+CoreDataProperties.m
//  Onthewaypro
//
//  Created by Rahul Sharma on 02/11/16.
//  Copyright © 2016 Rahul Sharma. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "ManageAddress+CoreDataProperties.h"

@implementation ManageAddress (CoreDataProperties)

@dynamic address;
@dynamic flatno;
@dynamic typeaddress;
@dynamic latit;
@dynamic longit;

@end
