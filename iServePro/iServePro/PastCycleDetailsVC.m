//
//  PastCycleDetailsVC.m
//  iServeProvider
//
//  Created by Rahul Sharma on 19/11/16.
//  Copyright © 2016 Rahul Sharma. All rights reserved.
//

#import "PastCycleDetailsVC.h"

@interface PastCycleDetailsVC ()

@end

@implementation PastCycleDetailsVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"Past Cycle Details";
    [self pastCycleDetails];
    // Do any additional setup after loading the view.
}

-(void)pastCycleDetails{
    _startDate.text = _pastCycleData[@"start_date"];
    _endDate.text = _pastCycleData[@"pay_date"];
    _acceptedbookings.text = _pastCycleData[@"no_of_bookings"];
    _openingBal.text = _pastCycleData[@"opening_balance"];
    _closedBal.text = _pastCycleData[@"closing_balance"];
    _earnings.text = _pastCycleData[@"booking_earn"];
    _paidAmount.text = _pastCycleData[@"pay_amount"];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
