//
//  ProviderTableViewCell.h
//  iServePro
//
//  Created by Rahul Sharma on 31/03/16.
//  Copyright © 2016 Rahul Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ProviderTableViewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *providerCell;
@property (strong, nonatomic)  NSString *providerType;

@end
