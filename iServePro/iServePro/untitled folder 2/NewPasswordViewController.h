//
//  NewPasswordViewController.h
//  Onthewaypro
//
//  Created by Rahul Sharma on 01/11/16.
//  Copyright © 2016 Rahul Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NewPasswordViewController : UIViewController

@property (strong, nonatomic) IBOutlet UITextField *password;
@property (strong, nonatomic) IBOutlet UITextField *conPassword;
- (IBAction)submitThePassword:(id)sender;

@property (strong, nonatomic) IBOutlet UIButton *submitButton;
@property (strong, nonatomic) IBOutlet UILabel *oldPasswordLabel;
@property (strong, nonatomic) IBOutlet UITextField *oldPasswordTF;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *passwordTopSpace;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *topspaceOfoldpasswordLabel;

@property (strong, nonatomic) IBOutlet NSLayoutConstraint *heightLabelOldPassword21;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *heightOfTextField;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *topSpaceForPasswordLabel;

@end
