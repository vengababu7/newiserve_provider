//
//  AddImagesCollectionViewCell.h
//  iServePro
//
//  Created by Rahul Sharma on 05/09/16.
//  Copyright © 2016 Rahul Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AddImagesCollectionViewCell : UICollectionViewCell
@property (strong, nonatomic) IBOutlet UIImageView *addImages;

@end
