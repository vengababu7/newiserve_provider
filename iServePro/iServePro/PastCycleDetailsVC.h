//
//  PastCycleDetailsVC.h
//  iServeProvider
//
//  Created by Rahul Sharma on 19/11/16.
//  Copyright © 2016 Rahul Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PastCycleDetailsVC : UIViewController

@property (strong , nonatomic)NSDictionary *pastCycleData;

@property (strong, nonatomic) IBOutlet UILabel *endDate;
@property (strong, nonatomic) IBOutlet UILabel *avgRating;
@property (strong, nonatomic) IBOutlet UILabel *acceptRate;

@property (strong, nonatomic) IBOutlet UILabel *startDate;
@property (strong, nonatomic) IBOutlet UILabel *earnings;
@property (strong, nonatomic) IBOutlet UILabel *openingBal;
@property (strong, nonatomic) IBOutlet UILabel *closedBal;
@property (strong, nonatomic) IBOutlet UILabel *acceptedbookings;
@property (strong, nonatomic) IBOutlet UILabel *rejectedBookings;
@property (strong, nonatomic) IBOutlet UILabel *ignoreBookings;
@property (strong, nonatomic) IBOutlet UILabel *paidAmount;
@end
