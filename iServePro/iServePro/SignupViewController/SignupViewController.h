//
//  SignupViewController.h
//  iServePro
//
//  Created by Rahul Sharma on 28/06/16.
//  Copyright © 2016 Rahul Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SignupViewController : UIViewController<UITextFieldDelegate,UITextViewDelegate>{
    BOOL isTnCButtonSelected;
}

@property (strong, nonatomic) IBOutlet UIImageView *profileImage;


- (IBAction)selectThePic:(id)sender;

@property (strong, nonatomic) IBOutlet UITextField *custNameTF;
@property (strong, nonatomic) IBOutlet UITextField *lastNameTF;
@property (strong, nonatomic) IBOutlet UITextField *emailTF;
@property (strong, nonatomic) IBOutlet UITextField *phoneTF;
@property (strong, nonatomic) IBOutlet UITextField *cityTF;
- (IBAction)selectProCity:(id)sender;
@property (strong, nonatomic) IBOutlet UIImageView *flagImage;
@property (strong, nonatomic) IBOutlet UILabel *countryCode;
@property (strong, nonatomic) IBOutlet UITextField *providerTF;
- (IBAction)selectProvider:(id)sender;
@property (strong, nonatomic) IBOutlet UITextField *passwordTF;

- (IBAction)selectCountryCode:(id)sender;
@property (strong, nonatomic) IBOutlet UITextField *confirmpasswordTF;
@property (strong, nonatomic) IBOutlet UIView *contentScrollView;
@property (strong, nonatomic) IBOutlet UIScrollView *mainScrollView;
- (IBAction)checkButton:(id)sender;
- (IBAction)termsNConditons:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *checkButton;
- (IBAction)signupAction:(id)sender;
@property (strong, nonatomic) IBOutlet UITextField *licenceNumber;

@end
