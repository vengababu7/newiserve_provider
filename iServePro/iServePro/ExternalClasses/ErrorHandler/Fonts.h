//
//  Fonts.h
//  Anypic
//
//  Created by rahul Sharma on 20/08/13.
//
//

#import <Foundation/Foundation.h>

@protocol Constant <NSObject>




//Fonts

#define     Robot_Black             @"Roboto-Black"
#define     Robot_Bold              @"Roboto-Bold"
#define     Robot_Light             @"Roboto-Light"
#define     Robot_Medium            @"Roboto-Medium"
#define     Robot_Regular           @"Roboto-Regular"
#define     Robot_Thin              @"Roboto-Thin"
#define     Robot_CondensedLight    @"RobotoCondensed-Light"
#define     Robot_CondensedRegular  @"RobotoCondensed-Regular"
#define     HELVETICANEUE_LIGHT     @"HelveticaNeue-Light"
#define     ZURICH_LIGHTCONDENSED   @"ZurichBT-LightCondensed"
#define     ZURICH_ROMAN            @"ZurichBT-Roman"
#define     ZURICH_LIGHTCONDENSED   @"ZurichBT-LightCondensed"
#define     ZURICH_ROMANCONDENSED   @"ZurichBT-RomanCondensed"
#define     ZURICH                  @"Zurich BT"
#define     LATO_REGULAR            @"Lato-Regular"
#define     LATO_BOLD               @"Lato-Bold"
#define     ZURICH_CONDENSED_BT     @"zurich-condensed-bt"
#define     ZURICH_CONDENSED     @"Zurich Condensed"
#define      Helvitica_Medium    @"HelveticaNeue-Medium"




#define CLEAR_COLOR     [UIColor clearColor]
#define WHITE_COLOR     [UIColor whiteColor]
#define BLACK_COLOR     [UIColor blackColor]
#define GREEN_COLOR     [UIColor greenColor]

@end