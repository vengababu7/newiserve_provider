//  ChatSocketIOClient.h
//  Sup
//
//  Created by Rahul Sharma on 1/8/16.
//  Copyright © 2016 3embed. All rights reserved.
//
#import<Foundation/Foundation.h>
#import "ChatSIOConfiguration.h"
#import "ChatSIOClient.h"
#import "Helper.h"


@protocol ChatSocketIODelegetes<NSObject>

-(void)responseFromChannels1:(NSDictionary *)responseDictionary;

-(void)didConnect;
-(void)didDisconnect;
-(void)messageSentSuccessfullyForMessageID:(NSMutableDictionary *)msgInfo;
-(void)didSubscribeToHistory;
-(void)didSubscribeToGetMessageAcks;
-(void)didSubscribetoOnlineStatus;

-(void)responseFromChannels:(NSDictionary *)responseDictionary;


@end

typedef NS_ENUM(NSUInteger, SocketMessageType) {
    SocketMessageTypeText,
    SocketMessageTypePhoto,
    SocketMessageTypeVideo,
    SocketMessageTypeLocation,
    SocketMessageTypeContact,
};

@interface ChatSocketIOClient : NSObject


+ (instancetype) sharedInstance;


-(void)socketIOSetup;
- (void) syncContacts:(NSArray *)contacts;
-(void)connectSocket;
-(void)disconnectSocket;

-(void)sendMessage:(NSData *)message fromUser:(NSString *)fromUserName toUser:(NSString *)toUserName withDocId:(NSString *)docID currentDate:(NSDate *)currentDate withTpe:(SocketMessageType)socketMessageType;


-(void)sendMessageAgain:(NSString *)message fromUser:(NSString*)fromUserName toUser:(NSString*)toUserName withDocId:(NSString*)docID currentDateId:(NSString*)currentID withType:(SocketMessageType)socketMessageType;

//-(void)sendReceivedAcknowledgement:(NSString *)fromUser withMessageID:(NSString *)messageID;
-(void)sendReceivedAcknowledgement:(NSString *)fromUser withMessageID:(NSString *)messageID ToReciver:(NSString*)toReciver docID:(NSString*)documentID messegStatus:(NSString *)msgStatus;

-(void)getMessageHistorySender:(NSString *)fromUser receiver:(NSString*)toUser;
-(void)getMessageAckHistorySender:(NSString *)fromUser receiver:(NSString*)toUser;

-(void)sendHeartBeatForUser:(NSString*)userName withStatus:(NSString *)status andPushToken:(NSString *)pushToken;

-(void)sendReadMessageStatus:(NSString *)msgStatus fromUser:(NSString*)fromUser toUser:(NSString*)toUser withMessageID:(NSString*)messageID;

-(void)sendOnlineStatustoServer:(NSString*)userName reciver:(NSString*)reciverName status:(NSString*)status currentDt:(NSString*)dt;
-(void)sendTypeingStatustoServer:(NSString *)recevierName;
-(void)getLastseenFromServer:(NSString *)reciverName;

-(void)sendUserNumforaAnyUpdate:(NSMutableArray *)usersNumbers;
-(void)broadCastProfileToserver:(NSString *)username;

/**
 *  @brief sending contacts to the socketIO
 *
 *  @param data Array containing the msisdn of the user to be called and the unique call id
 */
- (void) callUser:(NSDictionary *)data;

-(void)PublishToGetCallStatusEvent:(NSDictionary*)data;

/**
 *  @brief sending contacts to the socketIO
 *
 *  @param data Array containing the msisdn of the user to be called and the unique call id
 */
- (void) sendEvent:(NSDictionary *)data;
-(void) sendCallEndEvent:(NSDictionary*)data;
-(void)subscribeToCallEvent;



@property (nonatomic, weak) id <ChatSocketIODelegetes> chatDelegate;
@property (nonatomic, strong) NSMutableArray *channelsName;
//****iserve*******//
-(void)heartBeat:(NSString *)status;

@end


