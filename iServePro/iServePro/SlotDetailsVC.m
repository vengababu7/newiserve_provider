//
//  SlotDetailsVC.m
//  iServeProvider
//
//  Created by Rahul Sharma on 15/11/16.
//  Copyright © 2016 Rahul Sharma. All rights reserved.
//

#import "SlotDetailsVC.h"
#import "ScheduleImagesCell.h"
#import "LocationTracker.h"
#import "MTGoogleMapCustomURLInteraction.h"
#import <MessageUI/MessageUI.h>
#import "CustomerRatingPOPUP.h"
#import "ChatSIOClient.h"
#import "AmazonTransfer.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "ImageViewCollection.h"


@interface SlotDetailsVC ()<MFMessageComposeViewControllerDelegate,ratingPopDelegate,CLLocationManagerDelegate>
{
    GMSGeocoder *geocoder_;
    CustomerRatingPOPUP *cancelReason;
    ImageViewCollection *collectionImages;

}
@property(nonatomic,strong)CLLocationManager *locationManager;
@property(nonatomic,assign)double currentLatitude;
@property(nonatomic,assign)double currentLongitude;
@end

@implementation SlotDetailsVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationController.navigationBarHidden = NO;
    self.navigationItem.hidesBackButton           = NO;
    

    if([_dict[@"booked"]integerValue] == 3){
        self.navigationController.navigationBar.topItem.title =[NSString stringWithFormat:@"BID :%@",_slotData[@"bid"]];
    }else{
        self.navigationController.navigationBar.topItem.title =NSLocalizedString(@"SLOT DETAILS",@"SLOT DETAILS");
    }
    self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName: UIColorFromRGB(0x2598ED)};
    [[UINavigationBar appearance] setTitleTextAttributes:
     [NSDictionary dictionaryWithObjectsAndKeys:
      [UIFont fontWithName:@"OpenSans-regular" size:16.0],NSFontAttributeName,
      nil]];

    [self createNavLeftButton];
    
     _slotDate.text = _slotSelectedDate;
  
    _slotTime.text =[NSString stringWithFormat:@"%@ - %@", _dict[@"start_dt"],_dict[@"end_dt"]];
    if([_dict[@"booked"]integerValue] == 3){
        UIBarButtonItem *rightBarButtonItem = [[UIBarButtonItem alloc]
                                               initWithTitle:NSLocalizedString(@"CANCEL",@"CANCEL") style:UIBarButtonItemStylePlain
                                               target:self action:@selector(cancelSlot)];
        self.navigationItem.rightBarButtonItem = rightBarButtonItem;
        self.navigationItem.rightBarButtonItem.tintColor =UIColorFromRGB(0x2598ED);
        [self getSlotBookedDetails];
   }
    [self slotDetails];
    [self getCurrentLocation];
    // Do any additional setup after loading the view.
}



-(void)cancelSlot{
    cancelReason = [[CustomerRatingPOPUP alloc]init];
    UIWindow *window = [[UIApplication sharedApplication] keyWindow];
    [cancelReason onWindow:window];
    cancelReason.delegate = self;
}

-(void)popUpRatingOFfDismiss:(NSString *)Reason{
    [self cancelAppointment:Reason];
}

/**
 *  Booking Cancel Method
 *
 *  @param reason "ISERVEPRO" can be before he starts
 */

- (void)cancelAppointment:(NSString *)reason{
    
    self.navigationItem.leftBarButtonItem.enabled = NO;
    [[ProgressIndicator sharedInstance]showPIOnView:self.view withMessage:NSLocalizedString(@"Loading..",@"Loading..")];
    
    NSDictionary *parameters = @{
                                 kSMPcheckUserSessionToken: [[NSUserDefaults standardUserDefaults] objectForKey:KDAcheckUserSessionToken],
                                 kSMPCommonDevideId       :[[NSUserDefaults standardUserDefaults] objectForKey:kPMDDeviceIdKey],
                                 @"ent_reason"            :reason,
                                 @"ent_date_time"         :[Helper getCurrentDateTime],
                                 @"ent_bid":_slotData[@"bid"]
                                 };
    
    NetworkHandler *handler = [NetworkHandler sharedInstance];
    [handler composeRequestWithMethod:MethodabortAppointment
                              paramas:parameters
                         onComplition:^(BOOL succeeded, NSDictionary *response) {
                             [[ProgressIndicator sharedInstance]hideProgressIndicator];
                             
                             if (succeeded) {
                                 [self emitCanceltoSocket:reason];
                                  [self backToController];
                             }
                             else{
                                 NSLog(@"Error");
                             }
                         }];
}
-(void)emitCanceltoSocket:(NSString *)reason
{
    ChatSIOClient *socket = [ChatSIOClient sharedInstance];
    NSDictionary *message=@{
                            @"bid":_slotData[@"bid"],
                            @"bstatus":[NSNumber numberWithInt:10],
                            @"cid":_slotData[@"cid"],
                            @"proid":[[NSUserDefaults standardUserDefaults] objectForKey:@"ProviderId"],
                            @"dt":[Helper getCurrentDateTime],
                            @"ent_pro_reason":reason
                            };
    [socket publishToChannel:@"LiveBookingAck" message:message];
    
}

-(void)slotDetails{
    if ([_dict[@"booked"]integerValue] == 1 ) {
      [self.view bringSubviewToFront:self.availableSlotView];
        [self.availableSlotView setHidden:NO];
        [self.bookedSlotView setHidden:YES];
    }else if([_dict[@"booked"]integerValue] == 3){
        [self.view bringSubviewToFront:self.bookedSlotView];
        [self.availableSlotView setHidden:YES];
        [self.bookedSlotView setHidden:NO];
    }
}

/*-----------------------------------*/
#pragma mark - Navigation Bar methods
/*-----------------------------------*/
- (void)createNavLeftButton
{
    UIButton *navCancelButton =  [UIButton buttonWithType:UIButtonTypeCustom];
    [navCancelButton setImage:[UIImage imageNamed:@"current_booking_cross_icon_off"]
                     forState:UIControlStateNormal];
    [navCancelButton setImage:[UIImage imageNamed:@"current_booking_cross_icon_on"]
                     forState:UIControlStateSelected];
    
    [navCancelButton addTarget:self action:@selector(backToController) forControlEvents:UIControlEventTouchUpInside];
    [navCancelButton setFrame:CGRectMake(10.0f,0.0f,40,40)];
    
    // Create a container bar button
    UIBarButtonItem *containingcancelButton = [[UIBarButtonItem alloc] initWithCustomView:navCancelButton];
    
    UIBarButtonItem *negativeSpacer = [[UIBarButtonItem alloc]
                                       initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace
                                       target:nil action:nil];
    negativeSpacer.width = -16;// it was -6 in iOS 6
    [self.navigationItem setLeftBarButtonItems:[NSArray arrayWithObjects:negativeSpacer,containingcancelButton, nil] animated:NO];
}

-(void)backToController
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

-(void)getSlotBookedDetails{
    NSUserDefaults *ud =[NSUserDefaults standardUserDefaults];
    NSDictionary *dict = @{
                           @"ent_sess_token":[ud objectForKey:KDAcheckUserSessionToken],
                           @"ent_dev_id":[ud objectForKey:kPMDDeviceIdKey],
                               @"ent_slot_id":_dict[@"id"]
                           };
    NetworkHandler *handler =[NetworkHandler sharedInstance];
    [handler composeRequestWithMethod:@"GetBookedSlotDetail"
                              paramas:dict
                         onComplition:^(BOOL succeeded, NSDictionary *response) {
                             if (succeeded) {
                                 if ([response[@"errFlag"]integerValue]==0) {
                                     _slotData = response[@"data"];
                                     _custName.text =[NSString stringWithFormat:@"%@%@", _slotData[@"fname"], _slotData[@"lname"]];
                                     _address.text = _slotData[@"address"];
                                     _aboutDetails.text = _slotData[@"customer_notes"];
                                     if (_aboutDetails.text.length <3) {
                                         _aboutDetails.text = @"No Notes Provided By Cutomer";
                                     }
                                     [self.collection reloadData];
                                     
                                         self.navigationController.navigationBar.topItem.title =[NSString stringWithFormat:@"BID :%@",_slotData[@"bid"]];
                                 
                                     self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName: UIColorFromRGB(0x2598ED)};
                                     [[UINavigationBar appearance] setTitleTextAttributes:
                                      [NSDictionary dictionaryWithObjectsAndKeys:
                                       [UIFont fontWithName:@"OpenSans-regular" size:16.0],NSFontAttributeName,
                                       nil]];

                                 }
                             }
                         }];
    
}

- (IBAction)call:(id)sender {
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Call", @"Call") message:[NSString stringWithFormat:@"%@ ",_slotData[@"mobile"]] delegate:self cancelButtonTitle:NSLocalizedString(@"Cancel", @"Cancel") otherButtonTitles:NSLocalizedString(@"Call", @"Call"), nil];
    [alert show];
}
-(void)alertView:(UIAlertView *)alertVie didDismissWithButtonIndex:(NSInteger) buttonIndex
{
    if (buttonIndex == 0)
    {
        NSLog(@"Cancel Tapped.");
    }
    else if (buttonIndex == 1)
    {
        NSString *phoneNumber = _slotData[@"mobile"];
        NSString *phoneURLString = [NSString stringWithFormat:@"tel:%@", phoneNumber];
        NSURL *phoneURL = [NSURL URLWithString:phoneURLString];
        [[UIApplication sharedApplication] openURL:phoneURL];
    }
}

- (IBAction)message:(id)sender
{
   if ([MFMessageComposeViewController canSendText]) {
        MFMessageComposeViewController *message = [[MFMessageComposeViewController alloc] init];
        message.messageComposeDelegate = self;
        message.recipients=@[_slotData[@"mobile"]];
        [[message navigationBar] setTintColor:[UIColor blackColor]];
        message.navigationBar.titleTextAttributes = [NSDictionary dictionaryWithObject:[UIColor blackColor] forKey:NSForegroundColorAttributeName];
        
        [message setBody:@""];
        [self presentViewController:message animated:YES completion:nil];
   }
 
}

- (IBAction)navigate:(id)sender {
    LocationTracker *locaitontraker = [LocationTracker sharedInstance];
    float latitude = locaitontraker.lastLocaiton.coordinate.latitude;
    float longitude = locaitontraker.lastLocaiton.coordinate.longitude;
    CLLocationCoordinate2D position = CLLocationCoordinate2DMake(latitude, longitude);
    [self getAddress:position];
 
}
-(void)getCurrentLocation{
    if ([CLLocationManager locationServicesEnabled]) {
        
        if (!_locationManager) {
            _locationManager = [[CLLocationManager alloc] init];
            _locationManager.delegate = self;
            if ([_locationManager respondsToSelector:@selector(requestAlwaysAuthorization)]) {
                
                [_locationManager requestAlwaysAuthorization];
            }
            _locationManager.distanceFilter = kCLDistanceFilterNone;
            _locationManager.desiredAccuracy = kCLLocationAccuracyBest;
            
        }
        [_locationManager startUpdatingLocation];
    }
    else {
        
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Location Service",@"Location Service") message:NSLocalizedString(@"Unable to find your location,Please enable location services.",@"Unable to find your location,Please enable location services.") delegate:nil cancelButtonTitle:NSLocalizedString(@"Ok",@"Ok") otherButtonTitles:nil, nil];
        [alertView show];
        
    }
}
- (void)locationManager:(CLLocationManager *)manager
    didUpdateToLocation:(CLLocation *)newLocation
           fromLocation:(CLLocation *)oldLocation {
    
    [_locationManager stopUpdatingLocation];
    _currentLatitude = newLocation.coordinate.latitude;
    _currentLongitude = newLocation.coordinate.longitude;
    
    
}

#pragma mark UIAddress Formatter

- (void)getAddress:(CLLocationCoordinate2D)coordinate {
    
    if (!geocoder_) {
        geocoder_ = [[GMSGeocoder alloc] init];
    }
    
    
    GMSReverseGeocodeCallback handler = ^(GMSReverseGeocodeResponse *response,
                                          NSError *error) {
        if (response && response.firstResult) {
            
            GMSAddress *address = response.firstResult;
            NSString *start = [address.lines componentsJoinedByString:@","];
            
            [self performSelectorOnMainThread:@selector(openDirection:) withObject:start waitUntilDone:YES];
            
        }else {
            // NSLog(@"Could not reverse geocode point (%f,%f): %@",
            //coordinate.latitude, coordinate.longitude, error);
        }
    };
    
    [geocoder_ reverseGeocodeCoordinate:coordinate
                      completionHandler:handler];
    
}

- (void)openDirection:(NSString *)startLocation{
    
    NSString *destination =_slotData[@"address"];
    
    [MTGoogleMapCustomURLInteraction showDirections:@{DirectionsStartAddress: startLocation,
                                                      DirectionsEndAddress: destination,
                                                      DirectionsDirectionMode: @"Driving",
                                                      ShowMapKeyZoom: @"7",
                                                      ShowMapKeyViews: @"Satellite",
                                                      ShowMapKeyMapMode: @"standard"
                                                      }
                                      allowCallback:YES];
}

-(void)messageComposeViewController:(MFMessageComposeViewController *)controller didFinishWithResult:(MessageComposeResult)result {
    
    switch (result) {
        case MessageComposeResultCancelled:
            NSLog(@"You cancelled sending message");
            break;
        case MessageComposeResultFailed:
            NSLog(@"Message failed");
            break;
        case MessageComposeResultSent:
            NSLog(@"Message sent");
            break;
            
        default:
            NSLog(@"An error occurred while composing this message");
            break;
    }
    [self dismissViewControllerAnimated:YES completion:NULL];
}

#pragma uicollectionView delegates

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    if ([_slotData[@"job_images"] integerValue]==0) {
        [self.view bringSubviewToFront:self.noJobPhotos];
        [self.noJobPhotos setHidden:NO];
    }else{
        [self.noJobPhotos setHidden:YES];
        return [_slotData[@"job_images"] integerValue];
    }
    return [_slotData[@"job_images"] integerValue];
}


- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *cellIdentifier =@"slotsSchedule";
    ScheduleImagesCell *cell=[collectionView dequeueReusableCellWithReuseIdentifier:cellIdentifier forIndexPath:indexPath];
    
    NSString *strImageUrl = [NSString stringWithFormat:@"https://s3.amazonaws.com/%@/JobImages/%@_%ld.png",Bucket,_slotData[@"bid"],(long)indexPath.row];
    
    [cell.activityIndicator startAnimating];
    
    [cell.scheduledImage sd_setImageWithURL:[NSURL URLWithString:strImageUrl]
                       placeholderImage:[UIImage imageNamed:@"user_image_default"]
                              completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType,NSURL *imgUrl) {
                                  [cell.activityIndicator stopAnimating];
                             }];
    return cell;
}
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    collectionImages= [ImageViewCollection sharedInstance];
    UIWindow *window = [[UIApplication sharedApplication] keyWindow];
    [[NSUserDefaults standardUserDefaults] setObject:_slotData[@"bid"] forKey:@"BID"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    NSInteger profileTag =0;
    [collectionImages showPopUpWithDictionary:window jobImages:[_slotData[@"job_images"] integerValue] index:indexPath tag:profileTag];
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return CGSizeMake(66, 66);
}

- (IBAction)deleteSlot:(id)sender {
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Confirm", @"Confirm")
                                                        message:NSLocalizedString(@"Are you sure you want to Delete This Slot?", @"Are you sure you want to Delete This Slot?")
                                                       delegate:self
                                              cancelButtonTitle:NSLocalizedString(@"No", @"No")
                    
                                              otherButtonTitles:NSLocalizedString(@"Yes", @"Yes"), nil];
    alertView.tag = 99;
    [alertView show];

 }

/*-------------------------------*/
#pragma mark - UIAlertViewDelegate
/*-------------------------------*/
-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (alertView.tag == 99) {
        
        if (buttonIndex == 1)
        {
            [[ProgressIndicator sharedInstance]showPIOnView:self.view withMessage:@"Loading.."];
            NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
            NSDictionary *dict =@{
                                  @"ent_sess_token":[ud objectForKey:KDAcheckUserSessionToken],
                                  @"ent_dev_id":[ud objectForKey:kPMDDeviceIdKey],
                                  @"ent_pro_id":[ud objectForKey:@"ProviderId"],
                                  @"ent_slot_id":_dict[@"id"]
                                  };
            NetworkHandler *handler = [NetworkHandler sharedInstance];
            [handler composeRequestWithMethod:@"removeSlot"
                                      paramas:dict
                                 onComplition:^(BOOL succeeded, NSDictionary *response) {
                                     if (succeeded) {
                                         [[ProgressIndicator sharedInstance]hideProgressIndicator];
                                         if ([response[@"errFlag"] integerValue]==0) {
                                             [self backToController];
                                         }else{
                                             [Helper showAlertWithTitle:@"Message" Message:response[@"errMsg"]];
                                         }
                                     }
                                 }];
            
            
        }
    }
}

@end
