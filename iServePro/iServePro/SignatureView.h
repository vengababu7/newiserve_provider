//
//  SignatureView.h
//  iDeliver
//
//  Created by Rahul Sharma on 6/2/15.
//  Copyright (c) 2015 3Embed. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol SignatureViewDelegate<NSObject>

-(void)signatureMade;

@end
@interface SignatureView : UIView

@property(nonatomic, weak) id<SignatureViewDelegate> delegate;
- (void)erase;

@end
