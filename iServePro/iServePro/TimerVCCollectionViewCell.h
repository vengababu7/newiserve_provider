//
//  TimerVCCollectionViewCell.h
//  iServePro
//
//  Created by Rahul Sharma on 12/07/16.
//  Copyright © 2016 Rahul Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TimerVCCollectionViewCell : UICollectionViewCell

@property (strong, nonatomic) IBOutlet UIImageView *custImages;
@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;
@end
