//
//  CategoryDetailsUpdate.m
//  iServeProvider
//
//  Created by Rahul Sharma on 18/11/16.
//  Copyright © 2016 Rahul Sharma. All rights reserved.
//

#import "CategoryDetailsUpdate.h"

@interface CategoryDetailsUpdate ()
{
    UIBarButtonItem *rightBarButtonItem;
}
@end

@implementation CategoryDetailsUpdate

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"Category Details";
    [self getTheCategoryDetails];
    _categoryNameTF.userInteractionEnabled = NO;
    _curencyTF.userInteractionEnabled =NO;
    _visitFeeTF.userInteractionEnabled = NO;
    _priceMinTF.userInteractionEnabled=NO;
     _radiusTF.userInteractionEnabled=NO;
    _currencyLabel.text = @"Price Set By";
    [self createNavLeftButton];
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(dismissKeyboard)];
    [self.view addGestureRecognizer:tapGesture];
    // Do any additional setup after loading the view.
}
-(void)dismissKeyboard{
    [self.view endEditing:YES];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)getTheCategoryDetails{
    [[ProgressIndicator sharedInstance] showPIOnView:self.view withMessage:@"Loading.."];
    NSDictionary *dict =@{
                          @"ent_sess_token":[[NSUserDefaults standardUserDefaults] objectForKey:KDAcheckUserSessionToken],
                          @"ent_dev_id": [[NSUserDefaults standardUserDefaults] objectForKey:kPMDDeviceIdKey],
                          @"ent_cat_id":[[NSUserDefaults standardUserDefaults] objectForKey:@"Cat_ID"]
                          };
    NetworkHandler *handler = [NetworkHandler sharedInstance];
    [handler composeRequestWithMethod:@"GetCategoryDetail" paramas:dict
                         onComplition:^(BOOL succeeded, NSDictionary *response) {
                             if (succeeded) {
                                 [[ProgressIndicator sharedInstance]hideProgressIndicator];
                                 if ([response[@"errFlag"] integerValue]==0) {
                                     _dictCategory =  response[@"data"];
                                     [self categoryDetails:response[@"data"]];
                                     
                                 }else{
                                     [Helper showAlertWithTitle:@"Message" Message:response[@"errMsg"]];
                                 }
                             }
                         }];
}


-(void)categoryDetails:(NSDictionary *)dict
{
    _categoryNameTF.text = [NSString stringWithFormat:@"%@ (%@)",dict[@"cat_name"],dict[@"fee_type"]];
    _curencyTF.text = dict[@"price_set_by"];
   
    _radiusTF.text =@"100 miles";
    _price = dict[@"price_min"];
    _visit = dict[@"visit_fees"];
    NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];
    [formatter setNumberStyle:NSNumberFormatterCurrencyStyle];
    [formatter setLocale:[NSLocale currentLocale]];
    
    NSNumber *priceMin =[NSNumber numberWithFloat:[dict[@"price_min"] floatValue]];
    NSString *priceMinFee = [formatter stringFromNumber:priceMin];
     _priceMinTF.text = priceMinFee;

    NSNumber *visitFee =[NSNumber numberWithFloat:[dict[@"visit_fees"] floatValue]];
    NSString *visitFees = [formatter stringFromNumber:visitFee];
    _visitFeeTF.text = visitFees;
    
    if ([dict[@"price_set_by"] isEqualToString:@"Provider"]) {
        
        rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Edit",@"Edit") style:UIBarButtonItemStylePlain
                                                             target:self action:@selector(updateCategoryData)];
        self.navigationItem.rightBarButtonItem = rightBarButtonItem;
        self.navigationItem.rightBarButtonItem.tintColor =UIColorFromRGB(0x2598ED);
        
    }
    
}
-(void) createNavLeftButton
{
    UIImage *buttonImageNavNormal =[UIImage imageNamed:@"bnt_bck_normal.png"];
    UIImage *buttonImageNavHigh = [UIImage imageNamed:@"bnt_bck_pressed.png"];
    UIButton *cancelButton =  [UIButton buttonWithType:UIButtonTypeCustom];
    [cancelButton addTarget:self action:@selector(backButtonPressed) forControlEvents:UIControlEventTouchUpInside];
    [cancelButton setFrame:CGRectMake(0.0f,0.0f,buttonImageNavHigh.size.width,buttonImageNavHigh.size.height)];
    [cancelButton setFrame:CGRectMake(0.0f,0.0f,buttonImageNavNormal.size.width,buttonImageNavNormal.size.height)];
    [cancelButton setBackgroundImage:buttonImageNavHigh forState:UIControlStateHighlighted];
    [cancelButton setImage:buttonImageNavNormal forState:UIControlStateNormal];
    UIBarButtonItem *containingcancelButton = [[UIBarButtonItem alloc]
                                               initWithCustomView:cancelButton];
    UIBarButtonItem *negativeSpacer = [[UIBarButtonItem alloc]
                                       initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace
                                       target:nil action:nil];
    negativeSpacer.width = -5;// it was -6 in iOS 6  you can set this as per your preference
    [self.navigationItem setLeftBarButtonItems:[NSArray arrayWithObjects:negativeSpacer,containingcancelButton, nil] animated:NO];
}
-(void)backButtonPressed
{
    [self.navigationController popViewControllerAnimated:YES];
    
    [self.view endEditing:YES];
    
}

-(void)updateCategoryData
{
    _priceMinTF.userInteractionEnabled=YES;
    _radiusTF.userInteractionEnabled=YES;

      [Helper showAlertWithTitle:@"Message" Message:@"You Can Edit PricePerMinute and Radius"];
    rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Save",@"Save") style:UIBarButtonItemStylePlain
                                                         target:self action:@selector(updateServiceCategory)];
    self.navigationItem.rightBarButtonItem = rightBarButtonItem;
    self.navigationItem.rightBarButtonItem.tintColor =UIColorFromRGB(0x2598ED);
    
}

-(void)updateServiceCategory
{
     [[ProgressIndicator sharedInstance] showPIOnView:self.view withMessage:@"Loading.."];
    NSDictionary *dict =@{
                          @"ent_sess_token":[[NSUserDefaults standardUserDefaults] objectForKey:KDAcheckUserSessionToken],
                          @"ent_dev_id": [[NSUserDefaults standardUserDefaults] objectForKey:kPMDDeviceIdKey],
                          
                          @"ent_price_min":_price,
                          @"ent_pro_id": [[NSUserDefaults standardUserDefaults] objectForKey:@"ProviderId"],
                          @"ent_cat_id":[[NSUserDefaults standardUserDefaults] objectForKey:@"Cat_ID"],
                          @"ent_radius":_visit
                          };
    NetworkHandler *handler = [NetworkHandler sharedInstance];
    [handler composeRequestWithMethod:@"UpdateCategoryDetail"
                              paramas:dict
                         onComplition:^(BOOL succeeded, NSDictionary *response) {
                             if (succeeded) {
                                 [[ProgressIndicator sharedInstance]hideProgressIndicator];
                                 [Helper showAlertWithTitle:@"Message" Message:response[@"errMsg"]];
                                 [self.navigationController popViewControllerAnimated:YES];
                             }
                         }];
}
#pragma mark - UITextFeildDelegate

-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    if (textField == _priceMinTF) {
        _priceMinTF.text = @"";
        _price =@"";
    }else if(textField == _radiusTF){
        _radiusTF.text = @"";
        _visit =@"";
    }
     return YES;
}
- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    
}
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    if(textField==_priceMinTF || textField==_radiusTF){
        NSString *newString = [textField.text stringByReplacingCharactersInRange:range withString:string];
        
        NSString    *regex     = @"\\d{0,5}(\\.\\d{0,2})?";
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", regex];
        return [predicate evaluateWithObject:newString];
    }
    return YES;
    
}

-(void)textFieldDidEndEditing:(UITextField *)textField
{
    NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];
    [formatter setNumberStyle:NSNumberFormatterCurrencyStyle];
    [formatter setLocale:[NSLocale currentLocale]];
    if (textField ==  _priceMinTF) {
        _price = textField.text;
        NSNumber *priceMin =[NSNumber numberWithFloat:[textField.text floatValue]];
        NSString *priceMinFee = [formatter stringFromNumber:priceMin];
        _priceMinTF.text = priceMinFee;
        
    }else if(textField == _radiusTF){
        _visit = [NSString stringWithFormat:@"%@ miles",textField.text];
        NSNumber *visitFee =[NSNumber numberWithFloat:[textField.text floatValue]];
        NSString *visitFees = [formatter stringFromNumber:visitFee];
        _radiusTF.text = visitFees;
    }
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [self.view endEditing:YES];
    return YES;

}

@end
