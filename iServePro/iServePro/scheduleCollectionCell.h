//
//  scheduleCollectionCell.h
//  iServeProvider
//
//  Created by Rahul Sharma on 16/11/16.
//  Copyright © 2016 Rahul Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface scheduleCollectionCell : UICollectionViewCell
@property (strong, nonatomic) IBOutlet UILabel *scheduledSlots;

@end
