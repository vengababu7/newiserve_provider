//
//  DetailsScheduleViewController.m
//  Onthewaypro
//
//  Created by Rahul Sharma on 02/11/16.
//  Copyright © 2016 Rahul Sharma. All rights reserved.
//

#import "DetailsScheduleViewController.h"
#import "scheduleCollectionCell.h"
#import "User.h"
#import "iServeSplashController.h"
#import "LocationTracker.h"

@interface DetailsScheduleViewController ()
{
    BOOL fromTime;
    BOOL dateEnd;
    NSInteger repeatRnot;
    NSInteger weekDay;
    NSArray *slotArray;
     NSInteger selectedNoOfSlots;
}


@property (assign, nonatomic) BOOL fromDate;
@property (strong,nonatomic) NSString *radius;
@property (strong,nonatomic) NSString *price;
@property (strong,nonatomic) NSString *numSlots;
@property (strong,nonatomic) NSString *timeSlot;
@property (strong,nonatomic) NSString *slotStarts;
@property (strong,nonatomic) NSString *slotEnds;
@property (strong,nonatomic) NSString *timeStarts;

@end

@implementation DetailsScheduleViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    slotArray = @[@"30",@"60",@"90",@"120",@"150",@"180",@"210",@"240",@"270",@"300"];
    [self createNavLeftButton];
    self.title = @"ADD SLOTS";
    _slotsArray = [[NSMutableArray alloc]init];
    [_datePicker setMinimumDate: [NSDate date]];
    _everyDay.layer.borderWidth = 1;
    _everyDay.layer.borderColor = UIColorFromRGB(0X2598ED).CGColor;
    weekDay = 1;
    _repeatMode.selected = YES;
    repeatRnot = 2;
    _nonRepeat.selected = NO;
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(dismissKeyboard)];
    [self.view addGestureRecognizer:tapGesture];
    [_slotPickerContent reloadAllComponents];
    
    // Do any additional setup after loading the view.
}
-(void)dismissKeyboard{
    [self.view endEditing:YES];
}
-(void)viewDidAppear:(BOOL)animated{
    _addressLabel.text = flStrForObj([[NSUserDefaults standardUserDefaults] objectForKey:@"selectedAddress"]);
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
/*-------------------------*/
#pragma mark - UserDelegate
/*-------------------------*/
-(void)userDidLogoutSucessfully:(BOOL)sucess {
    if (sucess){
        // Logged it out Successfully
        NSLog(@"Logged it out Successfully");
    }
    else{
        // Session is Expired
        NSLog(@"Session is Expired");
    }
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:KDAcheckUserSessionToken];
    [[NSUserDefaults standardUserDefaults] synchronize];
    ProgressIndicator *pi = [ProgressIndicator sharedInstance];
    [pi hideProgressIndicator];
    [[LocationTracker sharedInstance] stopLocationTracking];
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:
                                @"Main" bundle:[NSBundle mainBundle]];
    
    iServeSplashController *splah = [storyboard instantiateViewControllerWithIdentifier:@"splash"];
    
    self.navigationController.viewControllers = [NSArray arrayWithObjects:splah, nil];
}

-(void)userDidFailedToLogout:(NSError *)error
{
    ProgressIndicator *pi = [ProgressIndicator sharedInstance];
    [pi hideProgressIndicator];
}

/*-----------------------------------*/
#pragma mark - Navigation Bar methods
/*-----------------------------------*/
-(void) createNavLeftButton
{
    UIImage *buttonImageNavNormal =[UIImage imageNamed:@"bnt_bck_normal.png"];
    UIImage *buttonImageNavHigh = [UIImage imageNamed:@"bnt_bck_pressed.png"];
    UIButton *cancelButton =  [UIButton buttonWithType:UIButtonTypeCustom];
    [cancelButton addTarget:self action:@selector(backButtonPressed) forControlEvents:UIControlEventTouchUpInside];
    [cancelButton setFrame:CGRectMake(0.0f,0.0f,buttonImageNavHigh.size.width,buttonImageNavHigh.size.height)];
    [cancelButton setFrame:CGRectMake(0.0f,0.0f,buttonImageNavNormal.size.width,buttonImageNavNormal.size.height)];
    [cancelButton setBackgroundImage:buttonImageNavHigh forState:UIControlStateHighlighted];
    [cancelButton setImage:buttonImageNavNormal forState:UIControlStateNormal];
    UIBarButtonItem *containingcancelButton = [[UIBarButtonItem alloc]
                                               initWithCustomView:cancelButton];
    UIBarButtonItem *negativeSpacer = [[UIBarButtonItem alloc]
                                       initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace
                                       target:nil action:nil];
    negativeSpacer.width = -5;// it was -6 in iOS 6  you can set this as per your preference
    [self.navigationItem setLeftBarButtonItems:[NSArray arrayWithObjects:negativeSpacer,containingcancelButton, nil] animated:NO];
}
-(void)backButtonPressed
{
    [self.navigationController popViewControllerAnimated:YES];
    [self.view endEditing:YES];
    
}


- (IBAction)goForNewAddress:(id)sender {
    
    [self performSegueWithIdentifier:@"toAddNewAddress" sender:nil];
}

- (IBAction)repeatMode:(id)sender {
    _heightOfendDate.constant = 24;
    repeatRnot = 2;
    _repeatMode.selected = YES;
    _nonRepeat.selected = NO;
    [_endDateLabel setHidden:NO];
    [_endDate setHidden:NO];
    _heightOfweekDays.constant = 45;
    [self.view layoutIfNeeded];
}

- (IBAction)nonRepeat:(id)sender {
    _heightOfendDate.constant = 0;
    repeatRnot = 1;
    _repeatMode.selected = NO;
    _nonRepeat.selected = YES;
    [_endDateLabel setHidden:YES];
    [_endDate setHidden:YES];
    _heightOfweekDays.constant = 0;
    [self.view layoutIfNeeded];
}

- (IBAction)startDate:(id)sender {
    [self.view endEditing:YES];
    dateEnd = NO;
    _fromDate = NO;
    _datePicker.datePickerMode = UIDatePickerModeDate;
    _selectType.text = @"SELECT START DATE";
    [self startAnimationUp];
}

- (IBAction)endDate:(id)sender {
    [self.view endEditing:YES];
    dateEnd = YES;
    _fromDate = NO;
    _datePicker.datePickerMode = UIDatePickerModeDate;
    _selectType.text = @"SELECT END DATE";
    [self startAnimationUp];
}
- (IBAction)startTime:(id)sender {
    [self.view endEditing:YES];
    if (!_timeSlot.length == 0 && !_numSlots.length == 0) {
        _fromDate = YES;
        _datePicker.datePickerMode = UIDatePickerModeTime;
        _selectType.text = @"SELECT START TIME";
        [self startAnimationUp];
        _slotsArray = [[NSMutableArray alloc]init];
    }else{
        [Helper showAlertWithTitle:@"Message" Message:@"Please Select The Slot Time and No of slot"];
    }
    
}

- (IBAction)cancelPicker:(id)sender {
    [self startAnimationDownforSlot];
    [self startAnimationDown];
}
- (IBAction)donePicker:(id)sender {
    
    [self startAnimationDown];
    if (_fromDate) {
        NSDateFormatter *dateFormat=[[NSDateFormatter alloc]init];
        dateFormat.dateStyle=kCFDateFormatterShortStyle;
        [dateFormat setDateFormat:@"hh:mm a"];//hh:mm a
        NSString *str=[NSString stringWithFormat:@"%@",[dateFormat  stringFromDate:_datePicker.date]];
        [_startTime setTitle:str forState:UIControlStateNormal];
        _timeStarts = str;
        [self createSlots:_datePicker.date];
    }else{
        NSDateFormatter *dateFormat=[[NSDateFormatter alloc]init];
        dateFormat.dateStyle=kCFDateFormatterShortStyle;
        [dateFormat setDateFormat:@"YYYY-MM-dd"];
        NSString *str=[NSString stringWithFormat:@"%@",[dateFormat  stringFromDate:_datePicker.date]];
        if (dateEnd) {
            [_endDate setTitle:str forState:UIControlStateNormal];
            _slotEnds = str;
        }else{
            [_startDate setTitle:str forState:UIControlStateNormal];
            _slotStarts = str;
        }
    }
}

-(void)createSlots:(NSDate *)myDate
{
    NSDate *dateEightHoursAhead;
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    double i = [_timeSlot floatValue]/60;
    double j = [_timeSlot floatValue]/60;
    for (i = j; i<=[_numSlots integerValue]*j; i+=j) {
        NSTimeInterval secondsInEightHours = i * 60 * 60;
        [formatter setDateFormat:@"hh:mm a"];  //HH:mm:ss
        
        if (dateEightHoursAhead) {
            _timeStarts =[formatter stringFromDate:dateEightHoursAhead];
        }
        dateEightHoursAhead = [myDate dateByAddingTimeInterval:secondsInEightHours];
        [_slotsArray addObject:[NSString stringWithFormat:@"%@    %@",_timeStarts,[formatter stringFromDate:dateEightHoursAhead]]];
    }
    [_slotsCollectionView reloadData];
}
-(void)startAnimationDownforSlot
{
    [UIView animateWithDuration:0.3
                          delay:0.1
                        options: UIViewAnimationOptionCurveEaseIn
                     animations:^{
                         _buttomSlotpickerView.constant = -180;
                         [self.view layoutIfNeeded];
                     }
                     completion:^(BOOL finished)
     {
         NSLog(@"Completed");
         
     }];
}
-(void)startAnimationUpforSlot
{
    
    [UIView animateWithDuration:0.3
                          delay:0.1
                        options: UIViewAnimationOptionCurveEaseOut
                     animations:^{
                         
                         _buttomSlotpickerView.constant = 1;
                         [self.view layoutIfNeeded];
                     }
                     completion:^(BOOL finished)
     {
         NSLog(@"Completed");
         
     }];
}

-(void)startAnimationDown
{
    [UIView animateWithDuration:0.3
                          delay:0.1
                        options: UIViewAnimationOptionCurveEaseIn
                     animations:^{
                         _bottomSpace.constant = -180;
                         [self.view layoutIfNeeded];
                     }
                     completion:^(BOOL finished)
     {
         NSLog(@"Completed");
         
     }];
}

-(void)startAnimationUp
{
    
    [UIView animateWithDuration:0.3
                          delay:0.1
                        options: UIViewAnimationOptionCurveEaseOut
                     animations:^{
                         
                         _bottomSpace.constant = 1;
                         [self.view layoutIfNeeded];
                     }
                     completion:^(BOOL finished)
     {
         NSLog(@"Completed");
         
     }];
}
#pragma uicollectionView delegates

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return _slotsArray.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *cellIdentifier =@"slots";
    scheduleCollectionCell *cell=[collectionView dequeueReusableCellWithReuseIdentifier:cellIdentifier forIndexPath:indexPath];
    cell.scheduledSlots.text = _slotsArray[indexPath.row];
    cell.scheduledSlots.layer.borderWidth = 1;
    cell.scheduledSlots.layer.borderColor = UIColorFromRGB(0XCCCCCC).CGColor;
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return CGSizeMake(66, 66);
}

- (IBAction)confirmSlot:(id)sender {
    [self addSlotApi];
}

-(void)addSlotApi{
    
    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
    NSDictionary *dict;
    if (repeatRnot == 1) {
        if (![ud objectForKey:@"AddressId"]) {
            [Helper showAlertWithTitle:@"Message" Message:@"Select The Location"];
        }else if ([_distanceInRadius.text isEqualToString: @""]){
            [Helper showAlertWithTitle:@"Message" Message:@"select the Distance"];
        }else if ([_pricePerTime.text isEqualToString: @""]){
            [Helper showAlertWithTitle:@"Message" Message:@"Select The Price"];
        }
        else if ([_noOfslots.text isEqualToString: @""]){
            [Helper showAlertWithTitle:@"Message" Message:@"Select The No Of Slots"];
        }
        else if ([_pricePerTime.text isEqualToString: @""]){
            [Helper showAlertWithTitle:@"Message" Message:@"Select The Slot Amount"];
        }
        else if(_slotStarts.length == 0){
            [Helper showAlertWithTitle:@"Message" Message:@"Select The Start Date"];
        }
        else if(_timeStarts.length == 0){
            [Helper showAlertWithTitle:@"Message" Message:@"Select The Start Time"];
        }else{
           [[ProgressIndicator sharedInstance]showPIOnView:self.view withMessage:@"Loading.."];
            dict = @{
                     @"ent_sess_token":[ud objectForKey:KDAcheckUserSessionToken],
                     @"ent_dev_id":[ud objectForKey:kPMDDeviceIdKey],
                     @"ent_pro_id":[ud objectForKey:@"ProviderId"],
                     @"ent_location":[ud objectForKey:@"AddressId"],
                     @"ent_start_time":[NSString stringWithFormat:@"%@ %@",_startDate.titleLabel.text,_startTime.titleLabel.text],
                     @"ent_radius":_radius,
                     @"ent_price":_price,
                     @"ent_duration":slotArray[selectedNoOfSlots],
                     @"ent_num_slot":_noOfslots.text,
                     @"ent_option":[NSString stringWithFormat:@"%d",repeatRnot]
                     };
            NetworkHandler *handler = [NetworkHandler sharedInstance];
            [handler composeRequestWithMethod:@"addslot"
                                      paramas:dict
                                 onComplition:^(BOOL succeeded, NSDictionary *response) {
                                     if (succeeded) {
                                         [[ProgressIndicator sharedInstance]hideProgressIndicator];
                                         if ([response[@"errFlag"] integerValue]==0) {
                                            // [ud removeObjectForKey:@"AddressId"];
                                             [Helper showAlertWithTitle:@"Message" Message:@" The requested slots have been created , please check your calendar"];
                                             [self.navigationController popViewControllerAnimated:YES];
                                             [self.view endEditing:YES];
                                         }else{
                                              [Helper showAlertWithTitle:@"Message" Message:response[@"errMsg"]];
                                         }
                                     }
                                 }];
        }
    }else{
        if (![ud objectForKey:@"AddressId"]) {
            [Helper showAlertWithTitle:@"Message" Message:@"Select The Location"];
        }else if ([_distanceInRadius.text isEqualToString: @""]){
            [Helper showAlertWithTitle:@"Message" Message:@"select the Distance"];
        }else if ([_pricePerTime.text isEqualToString: @""]){
            [Helper showAlertWithTitle:@"Message" Message:@"Select The Price"];
        }
        else if ([_noOfslots.text isEqualToString: @""]){
            [Helper showAlertWithTitle:@"Message" Message:@"Select The No Of Slots"];
        }
        else if ([_pricePerTime.text isEqualToString: @""]){
            [Helper showAlertWithTitle:@"Message" Message:@"Select The Slot Amount"];
        }
        else if(_slotStarts.length == 0){
            [Helper showAlertWithTitle:@"Message" Message:@"Select The Start Date"];
        }
        else if(_timeStarts.length == 0){
            [Helper showAlertWithTitle:@"Message" Message:@"Select The Start Time"];
        }
        else if(_slotEnds.length == 0){
            [Helper showAlertWithTitle:@"Message" Message:@"Select The End Date"];
        }else{
             [[ProgressIndicator sharedInstance]showPIOnView:self.view withMessage:@"Loading.."];
            dict = @{
                     @"ent_sess_token":[ud objectForKey:KDAcheckUserSessionToken],
                     @"ent_dev_id":[ud objectForKey:kPMDDeviceIdKey],
                     @"ent_pro_id":[ud objectForKey:@"ProviderId"],
                     @"ent_location":[ud objectForKey:@"AddressId"],
                     @"ent_start_time":[NSString stringWithFormat:@"%@ %@",_startDate.titleLabel.text,_startTime.titleLabel.text],
                     @"ent_radius":_radius,
                     @"ent_price":_price,
                     @"ent_duration":slotArray[selectedNoOfSlots],
                     @"ent_num_slot":_noOfslots.text,
                     @"ent_option":[NSString stringWithFormat:@"%ld",(long)repeatRnot],
                     @"ent_repeatday":[NSString stringWithFormat:@"%ld",(long)weekDay],
                     @"ent_end_date":[NSString stringWithFormat:@"%@",_endDate.titleLabel.text]
                     };
            NetworkHandler *handler = [NetworkHandler sharedInstance];
            [handler composeRequestWithMethod:@"addslot"
                                      paramas:dict
                                 onComplition:^(BOOL succeeded, NSDictionary *response) {
                                     if (succeeded) {
                                         [[ProgressIndicator sharedInstance]hideProgressIndicator];

                                         if ([response[@"errFlag"] integerValue]==0) {
                                           //  [ud removeObjectForKey:@"AddressId"];
                                             [Helper showAlertWithTitle:@"Message" Message:@" The requested slots have been created , please check your calendar"];
                                             [self.navigationController popViewControllerAnimated:YES];
                                             [self.view endEditing:YES];
                                         }else{
                                             if ([response[@"errNum"] integerValue]==83) {
                                                 User *logout = [User sharedInstance];
                                                 logout.delegate = self;
                                                 [logout logout];
                                             }
                                             [Helper showAlertWithTitle:@"Message" Message:response[@"errMsg"]];
                                         }

                                     }
                                      [[ProgressIndicator sharedInstance]hideProgressIndicator];
                                 }];
            
        }
    }
}

#pragma mark - TextFields

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    if (textField == _distanceInRadius) {
        _distanceInRadius.text = @"";
    }else if(textField == _pricePerTime){
        _pricePerTime.text = @"";
    }else if (textField == _noOfslots){
        _noOfslots.text = @"";
    }else if (textField == _slotMinutes){
        _slotMinutes.text = @"";
    }
    return YES;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    
}

- (BOOL)textFieldShouldEndEditing:(UITextField *)textField
{
    return YES;
}

-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    if(textField==_distanceInRadius || textField==_pricePerTime){
        NSString *newString = [textField.text stringByReplacingCharactersInRange:range withString:string];
        
        NSString    *regex     = @"\\d{0,5}(\\.\\d{0,2})?";
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", regex];
        return [predicate evaluateWithObject:newString];
    }else if (textField==_noOfslots){
        NSString *newString = [textField.text stringByReplacingCharactersInRange:range withString:string];
        NSString    *regex     = @"\\d{0,5}?";
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", regex];
        return [predicate evaluateWithObject:newString];
    }
    return YES;
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];
    [formatter setNumberStyle:NSNumberFormatterCurrencyStyle];
    [formatter setLocale:[NSLocale currentLocale]];
    
    if (textField == _distanceInRadius) {
        if (textField.text.length > 0) {
            _radius = textField.text;
            _distanceInRadius.text = [NSString stringWithFormat:@"%@ Miles",textField.text];
        }
       
    }else  if (textField == _pricePerTime){
        if (textField.text.length > 0) {
            _price = textField.text;
            NSNumber *amount =[NSNumber numberWithInteger:[textField.text integerValue]];
            NSString *price = [formatter stringFromNumber:amount];
            _pricePerTime.text = price;
        }
      
    }else if (textField == _noOfslots){
        if (textField.text.length > 0) {
            _numSlots = textField.text;
            _noOfslots.text = textField.text;
        }
       
        [self.view endEditing:YES];
    }
}

- (BOOL)textFieldShouldClear:(UITextField *)textField
{
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if (textField == _distanceInRadius) {
        [_distanceInRadius resignFirstResponder];
        [_pricePerTime becomeFirstResponder];
    }else if (textField == _pricePerTime){
        [_pricePerTime resignFirstResponder];
        [_noOfslots becomeFirstResponder];
    }else{
        [_noOfslots resignFirstResponder];
    }
    return YES;
}

- (IBAction)everyDay:(id)sender
{
    _everyDay.layer.borderWidth = 1;
    _everyDay.layer.borderColor = UIColorFromRGB(0X2598ED).CGColor;
    _weekDays.layer.borderColor = UIColorFromRGB(0Xffffff).CGColor;
    _weekEnds.layer.borderColor = UIColorFromRGB(0Xffffff).CGColor;
    weekDay = 1;
}

- (IBAction)doneSlotSelection:(id)sender {
    NSString *slotTime = [NSString stringWithFormat:@"%@ Minutes",slotArray[selectedNoOfSlots]];
    _timeSlot =slotArray[selectedNoOfSlots];
     [_noOfSlotButton setTitle:slotTime forState:UIControlStateNormal];
    [self startAnimationDownforSlot];
}

- (IBAction)weekDays:(id)sender {
    _weekDays.layer.borderWidth = 1;
    _weekDays.layer.borderColor = UIColorFromRGB(0X2598ED).CGColor;
    _everyDay.layer.borderColor = UIColorFromRGB(0Xffffff).CGColor;
    _weekEnds.layer.borderColor = UIColorFromRGB(0Xffffff).CGColor;
    weekDay = 2;
}

- (IBAction)weekEnds:(id)sender {
    _weekEnds.layer.borderWidth = 1;
    _weekEnds.layer.borderColor = UIColorFromRGB(0X2598ED).CGColor;
    _weekDays.layer.borderColor = UIColorFromRGB(0Xffffff).CGColor;
    _everyDay.layer.borderColor = UIColorFromRGB(0Xffffff).CGColor;
    weekDay = 3;
}

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}


- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    return [slotArray count];
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    return [slotArray objectAtIndex:row];
}
- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component{
    selectedNoOfSlots = row;
   
}
- (IBAction)selectSlots:(id)sender {
    [self.view endEditing:YES];
    [self startAnimationUpforSlot];
}

@end
