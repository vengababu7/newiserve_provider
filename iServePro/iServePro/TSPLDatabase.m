//
//  HMADatabase.m
//  Homappy
//
//  Created by Rahul Sharma on 03/08/15.
//  Copyright (c) 2015 Rahul Sharma. All rights reserved.
//

#import "TSPLDatabase.h"
#import "IServeAppDelegate.h"
#import "ManageAddress.h"

@implementation TSPLDatabase

/*------------------------------------------------------*/
#pragma mark - Generic Methods for Deleting row and Table
/*------------------------------------------------------*/

/**
*  Delete row from table
*
*  @param tableName EntityName
*  @param keys      Condition statement
*
*  @return returns YES if success, NO on Failure
*/

+ (BOOL)deleteArowFromTable:(NSString *)tableName andIndex:(NSInteger)index{
    IServeAppDelegate *appDelegate = (IServeAppDelegate *)[[UIApplication sharedApplication] delegate];
    NSManagedObjectContext *context = [appDelegate managedObjectContext];
    
    NSEntityDescription *entity1 = [NSEntityDescription entityForName:tableName inManagedObjectContext:context];
    NSFetchRequest *fetch = [[NSFetchRequest alloc] init];
    [fetch setEntity:entity1];
    
    NSError *fetchError;
    NSError *error;
    NSArray *fetchedProducts = [context executeFetchRequest:fetch error:&fetchError];
    
    NSManagedObject *managedObject = fetchedProducts[index];
    [context deleteObject:managedObject];
    
    if ([context save:&error])
    {
        NSLog(@"Row is Deleted from : %@",tableName);
        return YES;
    }
    else
    {
        return NO;
    }
    return NO;

    
}

+ (BOOL)deleteRowFromTable:(NSString*)tableName
                   andKeys:(NSString *)keys
{
    IServeAppDelegate *appDelegate = (IServeAppDelegate *)[[UIApplication sharedApplication] delegate];
    NSManagedObjectContext *context = [appDelegate managedObjectContext];
    
    NSEntityDescription *entity1 = [NSEntityDescription entityForName:tableName inManagedObjectContext:context];
    
    NSFetchRequest *fetch = [[NSFetchRequest alloc] init];
    [fetch setEntity:entity1];
    NSPredicate *pred = [NSPredicate predicateWithFormat:keys];
    [fetch setPredicate:pred];
    //... add sorts if you want them
    NSError *fetchError;
    NSError *error;
    NSArray *fetchedProducts = [context executeFetchRequest:fetch error:&fetchError];
    //    NSManagedObject *products = [fetchedProducts objectAtIndex:0];
    for (NSManagedObject *product in fetchedProducts) {
        [context deleteObject:product];
        // break;
    }
    
    if ([context save:&error])
    {
        NSLog(@"Row is Deleted from : %@",tableName);
        return YES;
    }
    else
    {
        return NO;
    }
    return NO;
}
/**
 *  Delete all the Details of table with Table name
 *
 *  @param tableName TableName
 */
+ (void)deleteAllRowsFromTable: (NSString *)tableName
{
    
    IServeAppDelegate *appDelegate = (IServeAppDelegate *)[[UIApplication sharedApplication] delegate];
    NSManagedObjectContext *context= [appDelegate managedObjectContext];
    
    NSEntityDescription *entity1=[NSEntityDescription entityForName:tableName inManagedObjectContext:context];
    
    NSFetchRequest *fetch=[[NSFetchRequest alloc]init];
    
    [fetch setEntity:entity1];
    NSError *fetchError;
    NSError *error;
    NSArray *fetchedProducts=[context executeFetchRequest:fetch error:&fetchError];
    
    for (NSManagedObject *product in fetchedProducts) {
        [context deleteObject:product];
    }
    [context save:&error];
}

/**
 *  Fetch the data from table with conditions
 *
 *  @param table     Entity name
 *  @param condition Condition statemen
 *  @param column    Column name
 *  @param asc       Order
 *
 *  @return ArrayOfResult
 */
+ (NSArray *)dataFromTable:(NSString *)table
                 condition:(NSString *)condition
                   orderBy:(NSString *)column
                 ascending:(BOOL)asc
{
    IServeAppDelegate *appDelegate = (IServeAppDelegate *)[[UIApplication sharedApplication] delegate];
    NSManagedObjectContext *context = [appDelegate managedObjectContext];
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    
    //[fetchRequest setReturnsObjectsAsFaults:NO];
    NSEntityDescription *entity = [NSEntityDescription entityForName:table inManagedObjectContext:context];
    [fetchRequest setEntity:entity];
    
    if (condition) {
        //NSLog(@" condition-->%@",condition);
        NSPredicate *predicate = [NSPredicate predicateWithFormat:condition];
        [fetchRequest setPredicate:predicate];
    }
    
    if (column) {
        NSSortDescriptor *sortDescriptor = [NSSortDescriptor sortDescriptorWithKey:column ascending:asc];
        [fetchRequest setSortDescriptors:[NSArray arrayWithObject:sortDescriptor]];
    }
    
    NSError *error = nil;
    NSArray *result = [context executeFetchRequest:fetchRequest error:&error];
    
    if (error)
    {
        NSLog(@"Core Data: %@", [error description]);
    }
    return result;
}
/**
 *  Fetch all the data from table
 *
 *  @param tableName EntityName
 *
 *  @return ArrayOfResult
 */
+ (NSArray *)getAllDataFromTable:(NSString *)tableName
{
    IServeAppDelegate *appDelegate = (IServeAppDelegate *)[[UIApplication sharedApplication] delegate];
    NSManagedObjectContext *context = [appDelegate managedObjectContext];
    
    NSEntityDescription *entity1 = [NSEntityDescription entityForName:tableName
                                               inManagedObjectContext:context];
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc]init];
    [fetchRequest setEntity:entity1];
    
    NSArray *result = [context executeFetchRequest:fetchRequest error:nil];
    
    NSLog(@"resutl : %@",result);
    return result;
}
/**
 *  Fetch the data from table with conditions
 *
 *  @param table     Entity name
 *  @param condition Condition statemen
 *
 *  @return ArrayOfResult
 */
+ (NSArray *)getAllDataFromTable:(NSString *)table
                    andCondition:(NSString *)condition
{
    IServeAppDelegate *appDelegate = (IServeAppDelegate *)[[UIApplication sharedApplication] delegate];
    NSManagedObjectContext *context = [appDelegate managedObjectContext];
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    
    //[fetchRequest setReturnsObjectsAsFaults:NO];
    NSEntityDescription *entity = [NSEntityDescription entityForName:table inManagedObjectContext:context];
    [fetchRequest setEntity:entity];
    
    if (condition) {
        //NSLog(@" condition-->%@",condition);
        NSPredicate *predicate = [NSPredicate predicateWithFormat:condition];
        [fetchRequest setPredicate:predicate];
    }
    
    NSError *error = nil;
    NSArray *result = [context executeFetchRequest:fetchRequest error:&error];
    
    if (error)
    {
        NSLog(@"Core Data: %@", [error description]);
    }
    return result;
}

/**
 *  more than one condition
 *
 *  @param BOOL condtion if success Returns
 *
 *  @return returns according the condition
 */
+ (NSArray *)getAllDataFromTable:(NSString *)table
                      condition1:(NSString *)condition1
                      condition2:(NSString *)condition2
{
    IServeAppDelegate *appDelegate = (IServeAppDelegate *)[[UIApplication sharedApplication] delegate];
    NSManagedObjectContext *context = [appDelegate managedObjectContext];
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    
    //[fetchRequest setReturnsObjectsAsFaults:NO];
    NSEntityDescription *entity = [NSEntityDescription entityForName:table inManagedObjectContext:context];
    [fetchRequest setEntity:entity];
    
    NSPredicate *predicate1 = [NSPredicate predicateWithFormat:condition1];;
    NSPredicate *predicate2 = [NSPredicate predicateWithFormat:condition2];
    NSArray *subPredicates = [NSArray arrayWithObjects:predicate1, predicate2, nil];
    
    NSPredicate *andPredicate = [NSCompoundPredicate andPredicateWithSubpredicates:subPredicates];
    
    [fetchRequest setPredicate:andPredicate];
    
    
    
    NSError *error = nil;
    NSArray *result = [context executeFetchRequest:fetchRequest error:&error];
    
    if (error)
    {
        NSLog(@"Core Data: %@", [error description]);
    }
    return result;
}


/**
 *  Address Table
 *
 *  @param dictionary Parameter to be stored
 *
 *  @return Bool value YES on Success or NO on Failure
 */
-(BOOL)makeDataEntryForAddressTable:(NSDictionary *)dictionary
{
    NSError *error;
    IServeAppDelegate *appDelegate = (IServeAppDelegate *)[[UIApplication sharedApplication] delegate];
    NSManagedObjectContext *context = [appDelegate managedObjectContext];
    
     ManageAddress *details = [NSEntityDescription insertNewObjectForEntityForName:@"ManageAddress" inManagedObjectContext:context];
    
    [details setAddress:flStrForObj([dictionary objectForKey:@"address"])];
    [details setFlatno:flStrForObj([dictionary objectForKey:@"flatno"])];
    [details setTypeaddress:flStrForObj([dictionary objectForKey:@"typeaddress"])];
    [details setLatit:flStrForObj([dictionary objectForKey:@"latit"])];
    [details setLongit:flStrForObj([dictionary objectForKey:@"longit"])];
    
    
    BOOL isSaved = [context save:&error];
    if (isSaved)
    {
        NSLog(@"Items are added to AddressTable Database");
    }
    else
    {
        NSLog(@"Failed to Add into AddressTable");
    }
    return isSaved;
}

@end
