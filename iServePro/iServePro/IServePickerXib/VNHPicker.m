//
//  Picker.m
//  iRush
//
//  Created by Rahul Sharma on 01/02/16.
//  Copyright © 2016 Rahul Sharma. All rights reserved.
//

#import "VNHPicker.h"

@implementation VNHPicker

@synthesize pickerView;
@synthesize arrayOfPicker;
@synthesize pickerTextField;


- (instancetype)initWithFrame:(CGRect)frame {
    
    
    self = [[[NSBundle mainBundle] loadNibNamed:@"VNHPicker"
                                          owner:self
                                        options:nil] firstObject];
    self = [super initWithFrame:frame];
    self.frameOfView = frame;
    return self;
}
- (void)loadPickerWithArray:(NSArray *)array
                     onView:(UIView *)pickerOn
                  forTextField:(UITextField *)selectedTextField {
    
    self.frame = self.frameOfView;
    [pickerOn addSubview:self];
    [self layoutIfNeeded];

    arrayOfPicker = [NSArray arrayWithArray:array];
    pickerTextField = (UITextField *)selectedTextField;
    pickerView.delegate = self;
    pickerView.dataSource = self;
}
// returns the number of 'columns' to display.
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    return 1;
}

// returns the # of rows in each component..
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    return arrayOfPicker.count;
}
// these methods return either a plain NSString, a NSAttributedString, or a view (e.g UILabel) to display the row for the component.
// for the view versions, we cache any hidden and thus unused views and pass them back for reuse.
// If you return back a different object, the old one will be released. the view will be centered in the row rect
- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    return arrayOfPicker[row][@"type_name"];
}
-(UIView *)pickerView:(UIPickerView *)pickerView viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(UIView *)view
{
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth(self.frame), 50)];
    label.backgroundColor = [UIColor whiteColor];
    label.textColor = [UIColor darkGrayColor];
   // label.font = [UIFont fontWithName:Roboto_Regular size:16];
    label.text = [NSString stringWithFormat:@"%@",arrayOfPicker[row][@"type_name"]];
    label.textAlignment = NSTextAlignmentCenter;
    return label;
}
- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component {
    
    pickerTextField.text = arrayOfPicker[row][@"type_name"];
    [self removeFromSuperview];
//    [self.delegate selectedArrayIndex:row
//                             andTitle:arrayOfPicker[row]];
}
@end
