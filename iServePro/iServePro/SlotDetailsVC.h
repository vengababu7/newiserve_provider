//
//  SlotDetailsVC.h
//  iServeProvider
//
//  Created by Rahul Sharma on 15/11/16.
//  Copyright © 2016 Rahul Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SlotDetailsVC : UIViewController

@property (strong, nonatomic) IBOutlet UIView *availableSlotView;
@property (strong, nonatomic) IBOutlet UIView *bookedSlotView;

@property (strong, nonatomic) IBOutlet UILabel *noJobPhotos;
@property (strong, nonatomic) IBOutlet UICollectionView *collection;

- (IBAction)call:(id)sender;
- (IBAction)message:(id)sender;
- (IBAction)navigate:(id)sender;
@property (strong, nonatomic) IBOutlet UILabel *address;
@property (strong, nonatomic) IBOutlet UILabel *custName;
@property (strong, nonatomic) IBOutlet UILabel *slotDate;
@property (strong, nonatomic) IBOutlet UILabel *slotTime;
@property (strong, nonatomic) IBOutlet UILabel *aboutDetails;
- (IBAction)deleteSlot:(id)sender;

@property (strong, nonatomic) NSDictionary *dict;
@property (strong, nonatomic) NSDictionary *slotData;

@property (strong, nonatomic) NSString *slotSelectedDate;
@end
