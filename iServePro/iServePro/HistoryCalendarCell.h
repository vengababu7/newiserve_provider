//
//  HistoryCalendarCell.h
//  iServePro
//
//  Created by Rahul Sharma on 28/03/16.
//  Copyright © 2016 Rahul Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HistoryCalendarCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *bookingStatus;
@property (strong, nonatomic) IBOutlet UILabel *bid;

@property (strong, nonatomic) IBOutlet UILabel *requestType;

@property (strong, nonatomic) IBOutlet UILabel *custName;
@property (strong, nonatomic) IBOutlet UILabel *custAddress;
@property (strong, nonatomic) IBOutlet UILabel *amountCharged;
@end
