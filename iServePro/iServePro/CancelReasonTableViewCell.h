//
//  CancelReasonTableViewCell.h
//  iServePro
//
//  Created by Rahul Sharma on 25/10/16.
//  Copyright © 2016 Rahul Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CancelReasonTableViewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *reasonLabel;

@end
