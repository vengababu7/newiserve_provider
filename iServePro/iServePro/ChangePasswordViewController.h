//
//  ChangePasswordViewController.h
//  iServeProvider
//
//  Created by Rahul Sharma on 21/11/16.
//  Copyright © 2016 Rahul Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ChangePasswordViewController : UIViewController

@property (strong, nonatomic) IBOutlet UITextField *newpasswordTF;
@property (strong, nonatomic) IBOutlet UITextField *oldpasswordTF;
@property (strong, nonatomic) IBOutlet UITextField *reenterPassword;
@property (strong, nonatomic) IBOutlet UIButton *submit;
- (IBAction)submit:(id)sender;
@end
