//
//  ScheduleImagesCell.h
//  iServeProvider
//
//  Created by Rahul Sharma on 16/11/16.
//  Copyright © 2016 Rahul Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ScheduleImagesCell : UICollectionViewCell
@property (strong, nonatomic) IBOutlet UIImageView *scheduledImage;

@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;
@end
