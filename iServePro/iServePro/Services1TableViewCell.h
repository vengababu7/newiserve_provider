//
//  Services1TableViewCell.h
//  Onthewaypro
//
//  Created by Rahul Sharma on 27/10/16.
//  Copyright © 2016 Rahul Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Services1TableViewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *serviceValue;

@property (strong, nonatomic) IBOutlet UILabel *serviceName;
@end
